import os
import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt

IMAGE_DIRECTORY = os.path.join(os.path.dirname(__file__), 'dataset', 'test')

generator = tf.keras.preprocessing.image.ImageDataGenerator(
	rotation_range=20,
	width_shift_range=0.2,
	height_shift_range=0.1,
	shear_range=0.2,
	zoom_range=[0.9, 1.2],
	horizontal_flip=False,
	fill_mode='constant',
	cval=255
)

sample_generator = generator.flow_from_directory(
	IMAGE_DIRECTORY,
	batch_size=20,
	target_size=(256, 256),
	shuffle=True
)

for (batch, cat) in sample_generator:
	for image in batch:
		plt.imshow(image.astype(int))
		fig = plt.draw()
		plt.waitforbuttonpress(0)
		plt.close(fig)

	# for image in batch:
	# 	for i in image:
	# 		for j in i:
	# 			print(j)
