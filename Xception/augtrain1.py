import os
import tensorflow as tf
from tensorflow import keras
import numpy as np

NUM_CLASSES = 42
OUTPUT_MODEL = "augtestmodel1.h5"

DIRECTORY = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'dataset', 'train', 'train')
PATIENCE = 10

def configure_gpu():
	gpus = tf.config.experimental.list_physical_devices('GPU')
	if gpus:
		try:
			# Currently, memory growth needs to be the same across GPUs
			for gpu in gpus:
				tf.config.experimental.set_memory_growth(gpu, True)
				logical_gpus = tf.config.experimental.list_logical_devices('GPU')
				print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
		except RuntimeError as e:
			# Memory growth must be set before GPUs have been initialized
			print(e)


def get_random_eraser(p=0.5, s_l=0.02, s_h=0.4, r_1=0.3, r_2=1/0.3, v_l=0, v_h=255, pixel_level=False):
    def eraser(input_img):
        img_h, img_w, img_c = input_img.shape
        p_1 = np.random.rand()

        if p_1 > p:
            return input_img

        while True:
            s = np.random.uniform(s_l, s_h) * img_h * img_w
            r = np.random.uniform(r_1, r_2)
            w = int(np.sqrt(s / r))
            h = int(np.sqrt(s * r))
            left = np.random.randint(0, img_w)
            top = np.random.randint(0, img_h)

            if left + w <= img_w and top + h <= img_h:
                break

        if pixel_level:
            c = np.random.uniform(v_l, v_h, (h, w, img_c))
        else:
            c = np.random.uniform(v_l, v_h)

        input_img[top:top + h, left:left + w, :] = c

        return tf.keras.applications.xception.preprocess_input(input_img)

    return eraser


def get_generators(
		batch_size=20,
		directory=DIRECTORY,
		reshape=299,
		validation_split=0.2
):
	# Add image augmentation parameters here
	generator_aug = tf.keras.preprocessing.image.ImageDataGenerator(
		rotation_range=20,
		width_shift_range=0.2,
		height_shift_range=0.1,
		shear_range=0.2,
		zoom_range=[0.9, 1.2],
		horizontal_flip=False,
		fill_mode='constant',
		cval=255,
		preprocessing_function=get_random_eraser(p=0.5, pixel_level=True),
		validation_split=validation_split
	)

	generator = tf.keras.preprocessing.image.ImageDataGenerator(
		preprocessing_function=tf.keras.applications.xception.preprocess_input,
		validation_split=validation_split
	)

	# Get generators
	train_generator = generator_aug.flow_from_directory(
		directory,
		batch_size=batch_size,
		target_size=(reshape, reshape),
		subset='training'
	)
	val_generator = generator.flow_from_directory(
		directory,
		batch_size=batch_size,
		target_size=(reshape, reshape),
		subset='validation'
	)

	return train_generator, val_generator


def build_model():
	# Instantiate an existing model for now
	base_model = tf.keras.applications.Xception(include_top=False, weights='imagenet')

	# Freeze all layers for now
	for layer in base_model.layers:
		layer.trainable = False

	# Additional layers
	x = base_model.output
	x = tf.keras.layers.GlobalAveragePooling2D()(x)
	x = tf.keras.layers.Dense(1024, activation='relu')(x)
	x = tf.keras.layers.Dropout(0.4)(x)
	outputs = tf.keras.layers.Dense(NUM_CLASSES, activation='softmax')(x)

	# Create model
	inputs = base_model.input
	model = tf.keras.Model(inputs=inputs, outputs=outputs)
	return model


def train_model(
		epochs=100,
		learning_rate=0.001,
		loss='categorical_crossentropy',
		model_file=OUTPUT_MODEL,
		patience=PATIENCE
):
	# Get generators
	train_generator, val_generator = get_generators()

	# # Build a dict of class weights
	# weights = {}
	# sizes = {}
	# total = 0
	# class_indices = train_generator.class_indices
	# for (subdir, index) in class_indices.items():
	# 	subdir_path = os.path.join(DIRECTORY, subdir)
	# 	file_list = next(os.walk(subdir_path))[2]
	# 	class_size = len(file_list)
	# 	sizes[index] = class_size
	# 	total += class_size
	# for (index, size) in sizes.items():
	# 	weights[index] = total / size / NUM_CLASSES

	# Saved model path
	if model_file is None:
		model_file = os.path.join(os.path.dirname(__file__), 'model.h5')

	model = build_model()

	# Compile model
	model.compile(
		optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate),
		loss=loss,
		metrics=['accuracy']
	)
	model.summary(line_length=160)

	# Train model
	model.fit(
		train_generator,
		validation_data=val_generator,
		epochs=epochs,
		# class_weight=weights,
		callbacks=[
			tf.keras.callbacks.EarlyStopping(monitor='val_accuracy', patience=patience),
            tf.keras.callbacks.ModelCheckpoint(model_file, monitor='val_accuracy', save_best_only=True)
		],
		verbose=2
	)


if __name__ == '__main__':
	configure_gpu()
	train_model()
