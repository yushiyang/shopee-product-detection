# Product Detection

The data `zip` file should be unzipped into a folder named `dataset` (which is ignored in `.gitignore`).

## Cloning

Please refer to https://git-lfs.github.com/ to activate LFS support before cloning the

## Setup

Once the venv is activated, execute in this directory `python -m pip install --upgrade pip` followed by `pip install -r requirements.txt`

This will install the packages locally to this directory

### Creating a new venv

In this directory in terminal, execute `python -m venv .` where `.` refers to the current directory

### Activating the venv

This is necessary each time you start a new terminal session.

On Linux, in this directory in terminal, execute `source Scripts/activate`

On Windows, in this directory, execute `Scripts/activate.bat` in cmd or execute `Scripts/Activate.ps1` in PowerShell

### Deactivating the venv

Anywhere in terminal, execute `deactivate`

## Running

Running `train.py` should not give any errors and should start the training.
