import os.path
import itertools
import time
import tensorflow as tf
import numpy as np

INPUT_MODEL = None  # Set to None if training a new model
INPUT_WEIGHTS = None
OUTPUT_MODEL = "model.h5"
CHECKPOINT_MODEL = "model.checkpoint.h5"
DIRECTORY = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'dataset', 'train', 'train')
COMPILE = True
# Set to True if training a new model, or changing any model parameter or hyperparameter except MONITOR_METRIC
# Set to False if continuing training from a checkpoint in the middle of an epoch
INITIAL_EPOCH = 0

# Model parameters
TRAIN_PREPROCESS = [tf.keras.applications.xception.preprocess_input]
VAL_PREPROCESS = [tf.keras.applications.xception.preprocess_input]
CLASSES = 42
SHAPE = (299, 299)
BASE_MODEL = tf.keras.applications.Xception(include_top=False, weights='imagenet')
TOP_LAYERS = [
	tf.keras.layers.GlobalAveragePooling2D(),
	tf.keras.layers.Dense(1024, activation='relu'),
	tf.keras.layers.Dropout(0.4),
	tf.keras.layers.Dense(CLASSES, activation='softmax')
]
FREEZE = 'base'

# Hyperparameters
LOSS_FUNCTION = 'categorical_crossentropy'
LEARNING_RATE = 0.001
OPTIMISER = tf.keras.optimizers.Adam(learning_rate=LEARNING_RATE)
METRICS = ['accuracy']
MONITOR_METRIC = 'val_accuracy'

# Training parameters
SUBSET = 0.01
VALIDATION_SPLIT = 0.2
BATCH_SIZE = 20
EPOCHS = 100
PATIENCE = 10
AUGMENTATION = {
	'rotation_range': 20,
	'width_shift_range': 0.2,
	'height_shift_range': 0.1,
	'shear_range': 0.2,
	'zoom_range': [0.9, 1.2],
	'horizontal_flip': False,
	'fill_mode': 'constant',
	'cval': 255
}
SEED = 1  # Random seed for shuffling training set

# Display parameters
LINE_LENGTH = 160


def configure_gpu():
	gpus = tf.config.experimental.list_physical_devices('GPU')
	if gpus:
		try:
			# Currently, memory growth needs to be the same across GPUs
			for gpu in gpus:
				tf.config.experimental.set_memory_growth(gpu, True)
				logical_gpus = tf.config.experimental.list_logical_devices('GPU')
				print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
		except RuntimeError as err:
			# Memory growth must be set before GPUs have been initialized
			print(err)


def custom_preprocess(image):
	return tf.keras.applications.xception.preprocess_input(image)

def get_generators(batch_size=BATCH_SIZE, directory=DIRECTORY, reshape=SHAPE, augmentation=AUGMENTATION, validation_split=VALIDATION_SPLIT, train_preprocess=TRAIN_PREPROCESS, val_preprocess=VAL_PREPROCESS, seed=SEED):
	# train_chain = lambda image, i: train_preprocess[-(i + 1)](train_chain(image, i + 1)) if i < len(train_preprocess) - 1 else train_preprocess[0]
	# def train_preprocessing(image, i):
	# 	def chain(image):
	# 		return train_preprocess[-(i + 1)]
	# 	return train_preprocessing(image, i + 1)

	# validation_split is deterministic across different ImageDataGenerators
	generator_aug = tf.keras.preprocessing.image.ImageDataGenerator(
		# Built-in image augmentations
		**augmentation,
		# Set preprocessing_function to custom_preprocess to chain preprocessings (or augmentation) step before the built-in preprocessing function
		preprocessing_function=tf.keras.applications.xception.preprocess_input,
		validation_split=validation_split
	)
	generator = tf.keras.preprocessing.image.ImageDataGenerator(
		preprocessing_function=tf.keras.applications.xception.preprocess_input,
		validation_split=validation_split
	)

	train_generator = generator_aug.flow_from_directory(directory, batch_size=batch_size, target_size=reshape, subset='training', seed=seed)
	val_generator = generator.flow_from_directory(directory, batch_size=batch_size, target_size=reshape, subset='validation', seed=seed)

	return (train_generator, val_generator)


def build_model(base_model=BASE_MODEL, top_layers=TOP_LAYERS, loss=LOSS_FUNCTION, optimiser=OPTIMISER, metrics=METRICS, freeze=FREEZE, compile=COMPILE):
	if not COMPILE:
		model = tf.keras.model.load_model(INPUT_MODEL)

	else:
		# Pretrained model as base
		inputs = base_model.input

		# Freeze all base layers
		if freeze == 'base'
			for layer in base_model.layers:
				layer.trainable = False

		# Additional layers
		x = base_model.output
		for layer in top_layers:
			x = layer(x)

		outputs = x

		# Create model
		model = tf.keras.Model(inputs=inputs, outputs=outputs)

		if INPUT_WEIGHTS:
			model.load_weights(INPUT_WEIGHTS)

		# Freeze top layers
		if freeze == 'top':
			for layer in model.layers[-1 * len(top_layers):]:
				layer.trainable = False

		model.compile(optimizer=optimiser, loss=loss, metrics=metrics)

	return model


def train_model(model_file=OUTPUT_MODEL, checkpoint_file=CHECKPOINT_MODEL, subset=SUBSET, epochs=EPOCHS, initial_epoch=INITIAL_EPOCH, patience=PATIENCE, learning_rate=LEARNING_RATE, monitor_metric=MONITOR_METRIC, line_length=LINE_LENGTH,
	batch_size=BATCH_SIZE, directory=DIRECTORY, reshape=SHAPE, augmentation=AUGMENTATION, validation_split=VALIDATION_SPLIT, train_preprocess=TRAIN_PREPROCESS, val_preprocess=VAL_PREPROCESS, seed=SEED,
	base_model=BASE_MODEL, top_layers=TOP_LAYERS, loss=LOSS_FUNCTION, optimiser=OPTIMISER, metrics=METRICS, freeze='base'
):
	(train_generator, val_generator) = get_generators(batch_size=batch_size, directory=directory, reshape=reshape, augmentation=augmentation, validation_split=validation_split, train_preprocess=train_preprocess, val_preprocess=val_preprocess, seed=seed)

	model = build_model(base_model=base_model, top_layers=top_layers, loss=loss, optimiser=optimiser, metrics=metrics, freeze='base')
	model.summary(line_length=line_length)

	# Train model
	print(time.ctime())
	model.fit(train_generator, validation_data=val_generator, epochs=epochs, initial_epoch=initial_epoch, steps_per_epoch=int(subset * len(train_generator)), callbacks=[
		# tf.keras.callbacks.ReduceLROnPlateau(),
		tf.keras.callbacks.EarlyStopping(monitor=monitor_metric, patience=patience),
		tf.keras.callbacks.ModelCheckpoint(model_file, monitor=monitor_metric, save_best_only=True),
		tf.keras.callbacks.ModelCheckpoint(checkpoint_file, monitor=monitor_metric, save_best_only=False, save_freq=100),
		# TqdmCallback(verbose=1),
		tf.keras.callbacks.LambdaCallback(on_epoch_end=lambda epoch, logs: print(time.ctime()))
	], verbose=2)


if __name__ == '__main__':
	configure_gpu()
	train_model()
