import os
import numpy as np
import tensorflow as tf

IMAGE_DIRECTORY = os.path.join(os.path.dirname(__file__), 'dataset', 'test')
INPUT_MODEL = os.path.join(os.path.dirname(__file__), 'Xception-2', 'augmodel3.h5')
OUTPUT = os.path.join(os.path.dirname(__file__), 'Xception-2', 'augmodel3.csv')

def run():
    model = tf.keras.models.load_model(INPUT_MODEL)
    generator = tf.keras.preprocessing.image.ImageDataGenerator(
        preprocessing_function=tf.keras.applications.inception_v3.preprocess_input
    )

    test_generator = generator.flow_from_directory(
        IMAGE_DIRECTORY,
        batch_size=20,
        target_size=(299, 299),
        shuffle=False
    )

    filenames = test_generator.filenames
    predictions = model.predict(test_generator)
    predictions = list(map(np.argmax, predictions))

    with open(OUTPUT, 'w') as file:
        file.write('filename,category\n')
        for filename, prediction in zip(filenames, predictions):
            if '(' not in filename:
                file.write(filename[5:] + ',' + str(prediction).zfill(2) + '\n')


if __name__ == '__main__':
    run()
